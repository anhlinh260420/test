<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
</head>
<body>
<?php 

require '../../connect.php';

$sql = "select * from tin_tuc
join the_loai on tin_tuc.ma_the_loai = the_loai.ma_the_loai";
$result = mysqli_query($connect,$sql);
?>
<a href="form_insert.php">
	Thêm
</a>
<table border="1" width="100%">
	<tr>
		<th>Tiêu  đề</th>
		<th>Tóm tắt</th>
		<th>Ảnh</th>
		<th>Thể loại</th>
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	<?php foreach ($result as $each): ?>
		<tr>
			<td>
				<?php echo $each['tieu_de'] ?>
			</td>
			<td>
				<?php echo $each['tom_tat'] ?>
			</td>
			<td>
				<img src="<?php echo $each['anh'] ?>" height='200'>
			</td>
			<td>
				<?php echo $each['ten_the_loai'] ?>
			</td>
			<td>
				<a href="form_update.php?ma_tin_tuc=<?php echo $each['ma_tin_tuc'] ?>">
					Sửa
				</a>
			</td>
			<td>
				<a href="delete.php?ma_tin_tuc=<?php echo $each['ma_tin_tuc'] ?>">
					Xóa
				</a>
			</td>
		</tr>
	<?php endforeach ?>
</table>
<?php mysqli_close($connect); ?>
</body>
</html>